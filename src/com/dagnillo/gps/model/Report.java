package com.dagnillo.gps.model;

import java.util.Date;

public class Report {
    private String loginCode;
    private Date reportDate;
    private  ReportType reportType;
    private Double latitude;
    private Double longitude;
    private Double gpsDop;
    private String textLabel;
    private String text;
    private Double altitude;
    private Double speed;

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReporDate(Date reporDate) {

        this.reportDate = reporDate;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {

        this.reportType = reportType;
    }

    public Double getLatitude() {

        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {

        return longitude;
    }

    public void setLongitude(double longitude) {

        this.longitude = longitude;
    }

    public Double getGpsDop() {

        return gpsDop;
    }

    public void setGpsDop(double gpsDop) {
        this.gpsDop = gpsDop;
    }

    public String getTextLabel() {

        return textLabel;
    }

    public void setTextLabel(String textLabel) {

        this.textLabel = textLabel;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }
}
