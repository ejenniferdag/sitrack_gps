package com.dagnillo.gps.reports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class ReportSender {

    private static final String URL = "http://test-externalrgw.ar.sitrack.com:5855/frame";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String METHOD_PUT = "PUT";


    public static Integer sendReport(String report, String authentication) throws IOException {

        Integer responseCode = null;

        HttpURLConnection connection = null;
        String response;
        InputStream in = null;   //recibir respuesta

        ByteArrayOutputStream responseStream = new ByteArrayOutputStream();

        try{
            connection = (HttpURLConnection) new URL(URL).openConnection();
            connection.addRequestProperty("Authorization", authentication);

            connection.addRequestProperty("Accept", CONTENT_TYPE_JSON);
            connection.setRequestMethod(METHOD_PUT);
            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);

            if(report != null){    //agrega el report
                connection.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
                connection.setRequestProperty("Content-Length", Integer.toString(report.length()));
                connection.setDoOutput(true);
                OutputStream out = connection.getOutputStream();
                out.write(report.getBytes());
                out.flush();
            }

            responseCode = connection.getResponseCode();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                in = connection.getInputStream();
            } else {
                in = connection.getErrorStream();
            }


            if(in != null){   //convierte valores en String y almacena en response
                byte[] buffer = new byte[1024];
                int readSize = 0;
                do{
                    responseStream.write(buffer, 0, readSize);
                    readSize = in.read(buffer);
                } while(readSize > 0);
            }
            response = responseStream.toString();

            System.out.println("El mensaje de respuesta es "+response);

        } catch (IOException ex) {
            throw ex;
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
        return responseCode;
    }

}
