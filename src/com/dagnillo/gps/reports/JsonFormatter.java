package com.dagnillo.gps.reports;

import com.dagnillo.gps.model.Report;
import java.text.SimpleDateFormat;

public class JsonFormatter {

    public static String getJSON (Report report){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"); //se usa formato ISO

        String result ="{\"loginCode\":\""+report.getLoginCode()+"\",\"reportDate\":\""+dateFormat.format(report.getReportDate())+"\",\"reportType\":\""+report.getReportType().getId()+"\",\"latitude\":"+report.getLatitude()+",\"longitude\":"+report.getLongitude()+",\"gpsDop\":"+report.getGpsDop()+",\"altitude\":"+report.getAltitude()+",\"speed\":"+report.getSpeed()+"}";

        return result;

    }
}
