package com.dagnillo.gps.reports;

import com.dagnillo.gps.model.Report;
import com.dagnillo.gps.model.ReportType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportGenerator {


    public static ReportGenerator instance;
    private final String DEFAULT_LATITUDE = "-32"; //Se quieren crear reports cercanos a este punto
    private final String DEFAULT_LONGITUDE = "-68"; //e quieren crear reports cercanos a este punto
    private List<String> reportTypeIds;
    private final String TEXT_LABEL= "TAG";
    private final String TEXT = "Jennifer DAgnillo";
    private final String LOGIN_CODE = "98173";
    private final Double ALTITUDE = new Double(0.0);

    private ReportGenerator(){
                //constructor
        reportTypeIds = new ArrayList<>();
        reportTypeIds.add("1");
        reportTypeIds.add("2");
        reportTypeIds.add("3");
        reportTypeIds.add("4");
        reportTypeIds.add("5");
        reportTypeIds.add("6");
        reportTypeIds.add("7");
        reportTypeIds.add("8");
        reportTypeIds.add("9");
        reportTypeIds.add("10");
        reportTypeIds.add("11");
        reportTypeIds.add("12");
        reportTypeIds.add("13");
        reportTypeIds.add("14");
        reportTypeIds.add("15");
        reportTypeIds.add("16");
        reportTypeIds.add("17");
        reportTypeIds.add("18");
        reportTypeIds.add("19");



    }

    public  static ReportGenerator getInstance(){
        if (instance == null){
            instance = new ReportGenerator();
        }
        return  instance;
    }

    /**
     * Método que permite generar un reporte, se considera la longitud y latitud cercanas a un punto fijo.
     * @return
     */

    public Report generateReport (){

        Report report = new Report();

        report.setLoginCode(LOGIN_CODE);

        report.setReporDate(new Date());

        int randomNumber = (int)(Math.random()*1000000);
        Double latitude = Double.valueOf(DEFAULT_LATITUDE+"."+String.valueOf(randomNumber));

        randomNumber = (int)(Math.random()*1000000);
        Double longitude = Double.valueOf(DEFAULT_LONGITUDE+"."+String.valueOf(randomNumber));

        report.setLatitude(latitude);
        report.setLongitude(longitude);

        Double gpsDop = Math.floor((Math.random())*2*10)/10;
        report.setGpsDop(gpsDop);

        ReportType reportType = new ReportType();
        int randomIndex = (int) (Math.random()*reportTypeIds.size());
        reportType.setId(reportTypeIds.get(randomIndex));

        report.setReportType(reportType);

        report.setText(TEXT);

        report.setTextLabel(TEXT_LABEL);

        report.setAltitude(ALTITUDE);

        Double speed = Math.floor((Math.random())*120*100)/100;
        report.setSpeed(speed);

        return report;
    }

}
