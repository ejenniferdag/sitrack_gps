package com.dagnillo.gps.reports;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


public class Authentication {

    private static final String APPLICATION = "ReportGeneratorTest";
    private static final String SECRET_KEY = "ccd517a1-d39d-4cf6-af65-28d65e192149";

    /**
     * Este método permite autenticar mediante la generación de signature codificada en base 64.
     */

    public static String requestAutentication () throws NoSuchAlgorithmException {

        String sWSAuth;

        String timestamp = String.valueOf(System.currentTimeMillis()/1000);

        byte[] parameters = (APPLICATION+SECRET_KEY+timestamp).getBytes();

        String signature = Base64.getEncoder().encodeToString(MessageDigest.getInstance("MD5").digest(parameters));

        sWSAuth = "SWSAuth application=\""+APPLICATION+"\",signature=\""+signature+"\",timestamp=\""+timestamp+"\"";

        return sWSAuth;


        // Authorization: SWSAuth application="MyTrackSystem", signature="lKD+lXU+iQjonvL1/c5hZw==",timestamp="1400599561"


    }
}
