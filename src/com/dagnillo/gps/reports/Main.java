package com.dagnillo.gps.reports;

import com.dagnillo.gps.model.Report;

public class Main {

    public static void main (String[] args) throws Exception {

        while (true) {

            String authentication = Authentication.requestAutentication();
            Report report = ReportGenerator.getInstance().generateReport();

            String jsonReport = JsonFormatter.getJSON(report);

            System.out.println("Se envía el siguiente reporte : " +jsonReport);

            Integer responseCode = ReportSender.sendReport(jsonReport, authentication);
            System.out.println(responseCode);

            if (responseCode!=200){

                if (responseCode==429 || responseCode>=500){
                    resendReportToSitrack(report);

                }else if (responseCode==400 || responseCode==401){
                    System.out.println("Error se corta la transmision y se deben revisar los mensajes de respuesta");
                    break;
                }else {
                    System.out.println("Otro tipo de error");
                    break;
                }
            }
            try {
                System.out.println("*********************************************************");
                Thread.sleep(60*1000); //Espera 60 segundos
            }catch( InterruptedException iex ){
                iex.printStackTrace();
            }

        }

    }

    private static void resendReportToSitrack (Report report)throws Exception {

        String jsonReport = JsonFormatter.getJSON(report);

        int count = 5;
        while (count>=0){

            try {
                Thread.sleep(10*1000);
            }catch( InterruptedException iex ){
                iex.printStackTrace();
            }

            String authentication = Authentication.requestAutentication();

            System.out.println("Se reenvia el siguiente reporte : " +jsonReport);

            Integer responseCode = ReportSender.sendReport(jsonReport, authentication);

            if (responseCode==200){
                break;
            }

            count = count-1;

            }
    }
}